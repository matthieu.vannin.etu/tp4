import Router from '../Router.js';
import Page from './Page.js';

export default class AddPizzaPage extends Page {
	render() {
		return /*html*/ `
			<form class="pizzaForm">
				<label>
					Nom :
					<input type="text" name="name">
				</label>
				<label>
				Image :<br/>
				<input type="text" name="image" placeholder="https://source.unsplash.com/xxxxxxx/600x600">
					<small>Vous pouvez trouver des images de pizza sur <a href="https://unsplash.com/">https://unsplash.com/</a> puis utiliser l'URL <code>https://source.unsplash.com/xxxxxxx/600x600</code> où <code>xxxxxxx</code> est l'id de la photo choisie (celui dans la barre d'adresse)</small>
				</label>
				<label>
					Prix petit format :
					<input type="number" name="price_small" step="0.05">
				</label>
				<label>
					Prix grand format :
					<input type="number" name="price_large" step="0.05">
				</label>
				<button type="submit">Ajouter</button>
			</form>`;
	}

	mount(element) {
		super.mount(element);
		const form = this.element.querySelector('.pizzaForm');
		form.addEventListener('submit', event => {
			event.preventDefault();
			this.submit();
		});
	}
	selector(inputName) {
		const theInput = this.element.querySelector(`input[name="${inputName}"]`);
		if (theInput === '') {
			alert(`Erreur : le champ ${inputName} est obligatoire`);
			return false;
		}
		return theInput.value;
	}

	submit() {
		// D.4. La validation de la saisie
		const name = this.selector('name');
		const image = this.selector('image');
		const price_small = this.selector('price_small');
		const price_large = this.selector('price_large');
		if (name && image && price_large && price_small) {
			const pizza = {
				name,
				image,
				price_small,
				price_large,
			};
			fetch('http://localhost:8080/api/v1/pizzas', {
				method: 'POST',
				headers: { 'Content-Type': 'application/json' },
				body: JSON.stringify(pizza),
			})
				.then(response => {
					if (response.ok) {
						alert(`La pizza ${name} a été ajoutée !`);
						this.element.querySelector('input[name="name"]').value = '';
						Router.navigate('/', true);
					} else {
						throw new Error();
					}
				})
				.catch(err => alert('Erreur'));
		}
	}
}
